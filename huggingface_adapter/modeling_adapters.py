class AdapterModel:
    """
    Base class for all adapter models
    """

    def freeze_base_model(self, freeze=True):
        """
        Freeze/unfreeze weights of base model (model without adapters).
        (Set requires_grad to `True` or `False`).

        Args:
            freeze (:obj:`bool`, optional, defaults to :obj:`True`):
                If true weights are freezed otherwise they are unfreezed.
        """
        for name, param in self.named_parameters():
            if not "adapter" in name.lower():
                param.requires_grad = not freeze

    def freeze_adapters(self, freeze=True):
        """
        Freeze/unfreeze weights of adapters.
        (Set requires_grad to `True` or `False`).

        Args:
            freeze (:obj:`bool`, optional, defaults to :obj:`True`):
                If true weights are freezed otherwise they are unfreezed.
        """
        for name, param in self.named_parameters():
            if "adapter" in name.lower():
                param.requires_grad = not freeze

    def freeze_model(self, freeze=True):
        """
        Freeze/unfreeze weights of entire model (base-model and adapters).
        (Set requires_grad to `True` or `False`).

        Args:
            freeze (:obj:`bool`, optional, defaults to :obj:`True`):
                If true weights are freezed otherwise they are unfreezed.
        """
        self.freeze_base_model(freeze)
        self.freeze_adapters(freeze)
