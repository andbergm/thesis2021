import math
import os
from queue import PriorityQueue
from typing import Optional

import numpy as np
import pandas as pd
import torch
import transformers
from tokenizers import Tokenizer
from tokenizers.models import WordLevel
from tokenizers.pre_tokenizers import Whitespace
from torch import Tensor, nn
from torch.nn import Embedding, Module, Transformer
from torch.utils.data import DataLoader, Dataset
from torchinfo import summary
from transformers import (
    Adafactor,
    AdamW,
    AlbertTokenizerFast,
    BertConfig,
    BertForMaskedLM,
    BertGenerationConfig,
    BertGenerationDecoder,
    BertModel,
    BertPreTrainedModel,
    BertTokenizerFast,
    EncoderDecoderModel,
    PretrainedConfig,
    PreTrainedTokenizerFast,
    RobertaTokenizerFast,
    Seq2SeqTrainer,
    Seq2SeqTrainingArguments,
)
from transformers.modeling_outputs import BaseModelOutputWithPoolingAndCrossAttentions

from huggingface_adapter import AdapterModel
from huggingface_adapter.adapter_albert import AdapterAlbertConfig, AdapterAlbertWithLengthPredict
from huggingface_adapter.adapter_bert import (
    AdapterBertConfig,
    AdapterBertForMaskedLM,
    AdapterBertModel,
    AdapterBertWithLengthPredict,
)
from huggingface_adapter.adapter_roberta import AdapterRobertaForMaskedLM
from huggingface_adapter.conditional_masked_lm import ConditionalMaskedLM
from huggingface_adapter.data import DataCollatorForConditionalMaskedLM
