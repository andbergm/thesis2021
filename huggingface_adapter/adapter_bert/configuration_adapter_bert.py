from transformers import BertConfig


class AdapterBertConfig(BertConfig):
    """
    Extention of the Huggingface :class:`BertConfig` class with added parameters for the adapter layer.

    See :class:`BertConfig` for documentation.

    Args:
        adapter_is_decoder (:obj:`bool`, `optional`, defaults to :obj:`False`):
            Whether the model is used as decoder or encoder.
            This changes the type of adapter that gets inserted into the model.
        adapter_hidden_size (:obj:`int`, `optional`, defaults to 128):
            For an encoder adapter (if :obj:`adapter_is_decoder` is `False`)
            this specifies the dimensionality of the hidden layer in the feedforwad layer of the adapter.
            For a decoder adapter (if :obj:`adapter_is_decoder` is `True`)
            this specifies the dimensionality of the hidden size in the the cross attention layer.
        adapter_intermediate_size (:obj:`int`, `optional`, defaults to 512):
            Dimensionality of the hidden layer of the feed forward layer in the decoder adapter.
            Only relevant if :obj:`adapter_is_decoder` is :obj:`True`.
        adapter_num_attention_heads (:obj:`int`, `optional`, defaults to 1):
            Number of attention heads in the cross attention layers in the decoder adapter.
            Only relevant if :obj:`adapter_is_decoder` is :obj:`True`
        adapter_layers (:obj:`List[int]`, `optional`):
            If provided, adapters are only inserted at the layers with number specified in the list.
            By default adapters are inserted into every layer of the BERT model.
        max_prediction_length (:obj:`int`):
            The size of the output vocabulary of the length token.
            Only relevant for the class :class:`AdapterBertWithLengthPredict`.
    """

    model_type = "adapter_bert"

    def __init__(
        self,
        adapter_is_decoder=False,
        adapter_hidden_size=128,
        adapter_intermediate_size=512,
        adapter_num_attention_heads=1,
        adapter_layers=None,
        max_prediction_length=128,
        **kwargs
    ):
        super().__init__(**kwargs)
        self.adapter_is_decoder = adapter_is_decoder
        self.adapter_hidden_size = adapter_hidden_size
        self.adapter_intermediate_size = adapter_intermediate_size
        self.adapter_num_attention_heads = adapter_num_attention_heads
        self.adapter_layers = adapter_layers
        self.max_prediction_length = max_prediction_length
