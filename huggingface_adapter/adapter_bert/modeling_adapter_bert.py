import torch
import torch.nn.functional as F
from torch import nn
from transformers.file_utils import ModelOutput
from transformers.models.bert.modeling_bert import (
    BertAttention,
    BertConfig,
    BertEmbeddings,
    BertEncoder,
    BertForMaskedLM,
    BertLayer,
    BertModel,
    BertOnlyMLMHead,
    BertPooler,
    BertPreTrainedModel,
)
from transformers.utils import logging

from .. import AdapterModel

logger = logging.get_logger(__name__)


class EncoderAdapter(nn.Module):
    def __init__(self, config):
        super().__init__()
        # self.LayerNorm = nn.LayerNorm(config.hidden_size, eps=config.layer_norm_eps)
        self.linear_down = nn.Linear(config.hidden_size, config.adapter_hidden_size)
        self.linear_up = nn.Linear(config.adapter_hidden_size, config.hidden_size)
        self.relu = nn.ReLU()

        self.layer_norm_after = nn.LayerNorm(config.hidden_size, eps=config.layer_norm_eps)

    def forward(self, hidden_states):
        hidden_states_down = self.linear_down(hidden_states)
        hidden_states_down = self.relu(hidden_states_down)
        hidden_states_up = self.linear_up(hidden_states_down)
        outputs = self.layer_norm_after(hidden_states_up + hidden_states)
        return outputs


class DecoderAdapter(nn.Module):
    def __init__(self, config):
        super().__init__()
        self.linear_down = nn.Linear(config.hidden_size, config.adapter_hidden_size)
        self.linear_up = nn.Linear(config.adapter_hidden_size, config.hidden_size)
        self.relu = nn.ReLU()

        # Cross attention
        # Create new config for adapter-cross-attention module
        cross_attention_config = BertConfig(config.to_dict())
        cross_attention_config.hidden_size = config.adapter_hidden_size
        cross_attention_config.num_attention_heads = config.adapter_num_attention_heads
        self.cross_attention = BertAttention(cross_attention_config)

        # Feed Forward
        self.linear_hidden_1 = nn.Linear(config.adapter_hidden_size, config.adapter_intermediate_size)
        self.linear_hidden_2 = nn.Linear(config.adapter_intermediate_size, config.adapter_hidden_size)

        self.layer_norm_after = nn.LayerNorm(config.hidden_size, eps=config.layer_norm_eps)

    def forward(self, hidden_states, attention_mask, encoder_hidden_states, encoder_attention_mask):

        hidden_states_down = self.linear_down(hidden_states)

        adapter_cross_attention_outputs = self.cross_attention(
            hidden_states=hidden_states_down,
            attention_mask=attention_mask,
            encoder_hidden_states=encoder_hidden_states,
            encoder_attention_mask=encoder_attention_mask,
        )
        adapter_cross_attention_outputs = adapter_cross_attention_outputs[0]

        adapter_hidden_states = self.linear_hidden_1(adapter_cross_attention_outputs)
        adapter_hidden_states = self.relu(adapter_hidden_states)
        adapter_hidden_states = self.linear_hidden_2(adapter_hidden_states)

        hidden_states_up = self.linear_up(adapter_hidden_states)
        outputs = self.layer_norm_after(hidden_states_up + hidden_states)
        return outputs


class AdapterBertLayer(BertLayer):
    """
    Bert Encoder Layer with added adapter.
    The added adapter is different, depending on whether the model is used as decoder or encoder.
    We add a new parameter to the config: :obj:`adapter_is_decoder`, that specifies the adapter to be added.
    As the pretrained model itself is an encoder bert model, the parameters :obj:`is_decoder` and :obj:`add_cross_attention` are are assumed to be false.
    """

    def __init__(self, config):
        assert config.is_decoder == False and config.add_cross_attention == False
        super().__init__(config)
        self.adapter_is_decoder = config.adapter_is_decoder
        if not self.adapter_is_decoder:
            self.encoderAdapter = EncoderAdapter(config)
        else:
            self.decoderAdapter = DecoderAdapter(config)

    def forward(
        self,
        hidden_states,
        attention_mask=None,
        head_mask=None,
        encoder_hidden_states=None,
        encoder_attention_mask=None,
        past_key_value=None,
        output_attentions=False,
    ):
        encoder_outputs = super().forward(hidden_states, attention_mask, head_mask, output_attentions, past_key_value)
        hidden_states = encoder_outputs[0]

        if self.adapter_is_decoder:
            output_hidden_states = self.decoderAdapter(
                hidden_states, attention_mask, encoder_hidden_states, encoder_attention_mask
            )
        else:
            output_hidden_states = self.encoderAdapter(hidden_states)

        return (output_hidden_states,) + encoder_outputs[1:]


class AdapterBertEncoder(BertEncoder):
    """
    Extention of the Huggingface Bert Model containing adapters.
    See :class:`AdapterBertModel` for documentation.
    """

    def __init__(self, config):
        nn.Module.__init__(self)
        self.config = config
        if config.adapter_layers is not None:
            # insert an adapter into layers specified by config.adapter_layers
            self.layer = nn.ModuleList(
                [
                    AdapterBertLayer(config) if i in config.adapter_layers else BertLayer(config)
                    for i in range(config.num_hidden_layers)
                ]
            )
        else:
            # Insert an adapter into every layer
            self.layer = nn.ModuleList([AdapterBertLayer(config) for _ in range(config.num_hidden_layers)])


class AdapterBertModel(BertModel, AdapterModel):
    """
    Extention of the Huggingface Bert Model containing adapters.
    The architecture of the adapters is adapted from the paper "Incorporating BERT into Parallel Sequence Decoding with Adapter".
    By default the adapters are inserted into every layer of the BERT encoder.
    If a list is provided as the :obj:`adapter_layers` argument of the configuration then the adapters are inserted only in the layers
    specified by the argument.
    """

    def __init__(self, config, add_pooling_layer=False):
        BertPreTrainedModel.__init__(self, config)

        self.config = config

        self.embeddings = BertEmbeddings(config)
        self.encoder = AdapterBertEncoder(config)  # Custom BertEncoder

        self.pooler = BertPooler(config) if add_pooling_layer else None

        self.init_weights()


class AdapterBertWithLengthPredict(AdapterBertModel):
    """
    Modification of the Adapter Bert Model, with length prediction.
    The maximum length has to be specified in the :obj:`max_prediction_length` attribute of the configuration.
    This model is not intended to be used as decoder! I.e. the arguments :obj:`is_decoder` and :obj:`adapter_is_decoder`
    in the provided configuration should be set to :obj:`False`.
    """

    def __init__(self, config):
        super().__init__(config)

        # We must include `adapter` in the name, to not freeze the parameter during adapter-training
        self.adapter_length_embeddings = nn.Embedding(config.max_prediction_length, config.hidden_size)

    def forward(
        self,
        input_ids=None,
        attention_mask=None,
        token_type_ids=None,
        position_ids=None,
        head_mask=None,
        inputs_embeds=None,
        return_dict=None,
    ):
        """
        Modified Bert forward method.
        A special length token is added to the input sequence.
        The output hidden state of this token is then used to decode the length.

        Return:
        :obj:`tuple(torch.FloatTensor)` comprising the following elements:
        last_hidden_state (:obj:`torch.FloatTensor` of shape :obj:`(batch_size, sequence_length, hidden_size)`):
            Sequence of hidden-states at the output of the last layer of the model.
            N.B. The hidden state of the internally added special length is not contained in this output sequence.
        pooler_output (:obj:`torch.FloatTensor`: of shape :obj:`(batch_size, hidden_size)`):
            Last layer hidden-state of the first token of the sequence (classification token)
            further processed by a Linear layer and a Tanh activation function. The Linear
            layer weights are trained from the next sentence prediction (classification)
            objective during pre-training.
        predicted_lengths (:obj:`torch.FloatTensor`: of shape :obj:`(batch_size, max_prediction_length)`):
            Decoded last layer hidden-state of the added length token.
        """
        return_dict = return_dict if return_dict is not None else self.config.use_return_dict

        if input_ids is not None and inputs_embeds is not None:
            raise ValueError("You cannot specify both input_ids and inputs_embeds at the same time")
        elif input_ids is not None:
            input_shape = input_ids.size()
            batch_size, seq_length = input_shape
        elif inputs_embeds is not None:
            input_shape = inputs_embeds.size()[:-1]
            batch_size, seq_length = input_shape
        else:
            raise ValueError("You have to specify either input_ids or inputs_embeds")

        device = input_ids.device if input_ids is not None else inputs_embeds.device

        if attention_mask is None:
            attention_mask = torch.ones(((batch_size, seq_length)), device=device)
        if token_type_ids is None:
            token_type_ids = torch.zeros(input_shape, dtype=torch.long, device=device)

        # Extend attention mask for length token
        if attention_mask.dim() == 3:
            attention_mask = torch.cat(
                [attention_mask.new_ones((batch_size, 1, attention_mask.size(2))), attention_mask], dim=1
            )
        else:
            attention_mask = torch.cat([attention_mask.new_ones((batch_size, 1)), attention_mask], dim=1)

        # We can provide a self-attention mask of dimensions [batch_size, from_seq_length, to_seq_length]
        # ourselves in which case we just need to make it broadcastable to all heads.
        extended_attention_mask: torch.Tensor = self.get_extended_attention_mask(attention_mask, input_shape, device)

        # Prepare head mask if needed
        # 1.0 in head_mask indicate we keep the head
        # attention_probs has shape bsz x n_heads x N x N
        # input head_mask has shape [num_heads] or [num_hidden_layers x num_heads]
        # and head_mask is converted to shape [num_hidden_layers x batch x num_heads x seq_length x seq_length]
        head_mask = self.get_head_mask(head_mask, self.config.num_hidden_layers)

        embedding_output = self.embeddings(
            input_ids=input_ids,
            position_ids=position_ids,
            token_type_ids=token_type_ids,
            inputs_embeds=inputs_embeds,
        )

        # Length tokens
        length_tokens = torch.zeros((batch_size, 1), dtype=torch.long, device=device)
        length_embedding_output = self.adapter_length_embeddings(length_tokens)
        embedding_output = torch.cat([length_embedding_output, embedding_output], dim=1)

        encoder_outputs = self.encoder(
            embedding_output,
            attention_mask=extended_attention_mask,
            head_mask=head_mask,
            return_dict=return_dict,
        )
        sequence_output = encoder_outputs[0]

        # remove length from sequence output
        length_hidden_state = sequence_output[:, 0, :]
        last_hidden_state = sequence_output[:, 1:, :]

        pooled_output = self.pooler(last_hidden_state) if self.pooler is not None else None

        # predict lengths
        lengths_logits = torch.matmul(
            length_hidden_state, self.adapter_length_embeddings.weight.transpose(0, 1)
        ).float()
        lengths_logits[:, 0] += float("-inf")  # Cannot predict the 0 (the len_token)

        if not return_dict:
            return last_hidden_state, pooled_output, lengths_logits

        return BertWithLengthPreditOutput(
            last_hidden_state=last_hidden_state, pooler_output=pooled_output, lengths_logits=lengths_logits
        )


class BertWithLengthPreditOutput(ModelOutput):
    """
    Base class for model's outputs that also contains a pooling of the last hidden states.

    Args:
        last_hidden_state (:obj:`torch.FloatTensor` of shape :obj:`(batch_size, sequence_length, hidden_size)`):
            Sequence of hidden-states at the output of the last layer of the model.
            N.B. The hidden state of the internally added special length is not contained in this output sequence.
        pooler_output (:obj:`torch.FloatTensor`: of shape :obj:`(batch_size, hidden_size)`):
            Last layer hidden-state of the first token of the sequence (classification token)
            further processed by a Linear layer and a Tanh activation function. The Linear
            layer weights are trained from the next sentence prediction (classification)
            objective during pre-training.
        lengths_logits (:obj:`torch.FloatTensor`: of shape :obj:`(batch_size, max_prediction_length)`):
            Decoded last layer hidden-state of the added length token.
    """

    last_hidden_state: torch.FloatTensor = None
    pooler_output: torch.FloatTensor = None
    lengths_logits: torch.FloatTensor = None


class AdapterBertForMaskedLM(BertForMaskedLM, AdapterModel):
    """Adapter Bert Model with a `language modeling` head on top."""

    def __init__(self, config):
        super().__init__(config)

        self.bert = AdapterBertModel(config, add_pooling_layer=False)
        self.cls = BertOnlyMLMHead(config)

        self.init_weights()
