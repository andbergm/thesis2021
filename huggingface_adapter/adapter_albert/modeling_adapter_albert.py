import torch
import torch.nn.functional as F
from torch import nn
from transformers.models.albert.modeling_albert import (
    AlbertEmbeddings,
    AlbertLayer,
    AlbertLayerGroup,
    AlbertModel,
    AlbertPreTrainedModel,
    AlbertTransformer,
)

from .. import AdapterModel
from ..adapter_bert.modeling_adapter_bert import (
    AdapterBertEncoder,
    BertWithLengthPreditOutput,
    DecoderAdapter,
    EncoderAdapter,
)


class AdapterAlbertLayer(AlbertLayer):
    """
    Albert Encoder Layer with added adapter.
    The Implementation has been copied from :class:`AdapterBertLayer`.
    """

    def __init__(self, config):
        assert config.is_decoder == False and config.add_cross_attention == False
        super().__init__(config)
        self.adapter_is_decoder = config.adapter_is_decoder
        if not self.adapter_is_decoder:
            self.encoderAdapter = EncoderAdapter(config)
        else:
            self.decoderAdapter = DecoderAdapter(config)

    def forward(
        self,
        hidden_states,
        attention_mask=None,
        head_mask=None,
        encoder_hidden_states=None,
        encoder_attention_mask=None,
        output_attentions=False,
    ):
        encoder_outputs = super().forward(hidden_states, attention_mask, head_mask, output_attentions)
        hidden_states = encoder_outputs[0]

        if self.adapter_is_decoder:
            output_hidden_states = self.decoderAdapter(
                hidden_states, attention_mask, encoder_hidden_states, encoder_attention_mask
            )
        else:
            output_hidden_states = self.encoderAdapter(hidden_states)

        return (output_hidden_states,) + encoder_outputs[1:]


class AdapterAlbertLayerGroup(AlbertLayerGroup):
    def __init__(self, config):
        super().__init__(config)

        self.albert_layers = nn.ModuleList([AdapterAlbertLayer(config) for _ in range(config.inner_group_num)])


class AdapterAlbertTransformer(AlbertTransformer):
    def __init__(self, config):
        super().__init__(config)

        self.config = config
        self.embedding_hidden_mapping_in = nn.Linear(config.embedding_size, config.hidden_size)
        self.albert_layer_groups = nn.ModuleList(
            [AdapterAlbertLayerGroup(config) for _ in range(config.num_hidden_groups)]
        )


class AdapterAlbertModel(AlbertModel, AdapterModel):
    def __init__(self, config, add_pooling_layer=False):
        AlbertPreTrainedModel.__init__(self, config)

        self.config = config

        self.embeddings = AlbertEmbeddings(config)

        self.encoder = AdapterAlbertTransformer(config)
        if add_pooling_layer:
            self.pooler = nn.Linear(config.hidden_size, config.hidden_size)
            self.pooler_activation = nn.Tanh()
        else:
            self.pooler = None
            self.pooler_activation = None

        self.init_weights()


class AdapterAlbertWithLengthPredict(AdapterAlbertModel):
    """
    Modification of the Adapter Albert Model, with length prediction.
    The Implementation has been copied and adapted from :class:`AdapterBertWithLengthPredict`.
    """

    def __init__(self, config):
        super().__init__(config)

        # We must include `adapter` in the name, to not freeze the parameter during adapter-training

        self.adapter_length_embeddings = nn.Embedding(config.max_prediction_length, config.embedding_size)
        self.adapter_hidden_to_embedding = nn.Linear(config.hidden_size, config.embedding_size)

    def forward(
        self,
        input_ids=None,
        attention_mask=None,
        token_type_ids=None,
        position_ids=None,
        head_mask=None,
        inputs_embeds=None,
        return_dict=None,
    ):
        """
        Modified Albert forward method.
        A special length token is added to the input sequence.
        The output hidden state of this token is then used to decode the length.

        Return:
        :obj:`tuple(torch.FloatTensor)` comprising the following elements:
        last_hidden_state (:obj:`torch.FloatTensor` of shape :obj:`(batch_size, sequence_length, hidden_size)`):
            Sequence of hidden-states at the output of the last layer of the model.
            N.B. The hidden state of the internally added special length is not contained in this output sequence.
        pooler_output (:obj:`torch.FloatTensor`: of shape :obj:`(batch_size, hidden_size)`):
            Last layer hidden-state of the first token of the sequence (classification token)
            further processed by a Linear layer and a Tanh activation function. The Linear
            layer weights are trained from the next sentence prediction (classification)
            objective during pre-training.
        predicted_lengths (:obj:`torch.FloatTensor`: of shape :obj:`(batch_size, max_prediction_length)`):
            Decoded last layer hidden-state of the added length token.
        """
        return_dict = return_dict if return_dict is not None else self.config.use_return_dict

        if input_ids is not None and inputs_embeds is not None:
            raise ValueError("You cannot specify both input_ids and inputs_embeds at the same time")
        elif input_ids is not None:
            input_shape = input_ids.size()
            batch_size, seq_length = input_shape
        elif inputs_embeds is not None:
            input_shape = inputs_embeds.size()[:-1]
            batch_size, seq_length = input_shape
        else:
            raise ValueError("You have to specify either input_ids or inputs_embeds")

        device = input_ids.device if input_ids is not None else inputs_embeds.device

        if attention_mask is None:
            attention_mask = torch.ones(((batch_size, seq_length)), device=device)
        if token_type_ids is None:
            token_type_ids = torch.zeros(input_shape, dtype=torch.long, device=device)

        # Extend attention mask for length token
        if attention_mask.dim() == 3:
            attention_mask = torch.cat(
                [attention_mask.new_ones((batch_size, 1, attention_mask.size(2))), attention_mask], dim=1
            )
        else:
            attention_mask = torch.cat([attention_mask.new_ones((batch_size, 1)), attention_mask], dim=1)

        # We can provide a self-attention mask of dimensions [batch_size, from_seq_length, to_seq_length]
        # ourselves in which case we just need to make it broadcastable to all heads.
        extended_attention_mask: torch.Tensor = self.get_extended_attention_mask(attention_mask, input_shape, device)

        # Prepare head mask if needed
        # 1.0 in head_mask indicate we keep the head
        # attention_probs has shape bsz x n_heads x N x N
        # input head_mask has shape [num_heads] or [num_hidden_layers x num_heads]
        # and head_mask is converted to shape [num_hidden_layers x batch x num_heads x seq_length x seq_length]
        head_mask = self.get_head_mask(head_mask, self.config.num_hidden_layers)

        embedding_output = self.embeddings(
            input_ids=input_ids,
            position_ids=position_ids,
            token_type_ids=token_type_ids,
            inputs_embeds=inputs_embeds,
        )

        # Length tokens
        length_tokens = torch.zeros((batch_size, 1), dtype=torch.long, device=device)
        length_embedding_output = self.adapter_length_embeddings(length_tokens)
        embedding_output = torch.cat([length_embedding_output, embedding_output], dim=1)

        encoder_outputs = self.encoder(
            embedding_output,
            attention_mask=extended_attention_mask,
            head_mask=head_mask,
            return_dict=return_dict,
        )
        sequence_output = encoder_outputs[0]

        # remove length from sequence output
        length_hidden_state = sequence_output[:, 0, :]
        last_hidden_state = sequence_output[:, 1:, :]

        # This line is different in the `AdapterBertWithLengthPredict` implementation,
        # as there the pooler is implemented as a separate module. The functionality is the same though.
        pooled_output = (
            self.pooler_activation(self.pooler(last_hidden_state[:, 0])) if self.pooler is not None else None
        )

        # predict lengths
        lengths_logits = torch.matmul(
            length_hidden_state, self.adapter_length_embeddings.weight.transpose(0, 1)
        ).float()
        lengths_logits[:, 0] += float("-inf")  # Cannot predict the 0 (the len_token)

        if not return_dict:
            return last_hidden_state, pooled_output, lengths_logits

        return AlbertWithLengthPreditOutput(
            last_hidden_state=last_hidden_state, pooler_output=pooled_output, lengths_logits=lengths_logits
        )


AlbertWithLengthPreditOutput = BertWithLengthPreditOutput
