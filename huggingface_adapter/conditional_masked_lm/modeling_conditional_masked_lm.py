from dataclasses import dataclass
from typing import Optional, Tuple, Union

import torch
import torch.nn.functional as F
from torch import nn
from transformers import PreTrainedModel
from transformers.file_utils import ModelOutput
from transformers.modeling_outputs import Seq2SeqLMOutput
from transformers.utils import logging

from .. import AdapterModel
from ..adapter_albert import AdapterAlbertConfig, AdapterAlbertWithLengthPredict
from ..adapter_bert import AdapterBertConfig, AdapterBertForMaskedLM, AdapterBertWithLengthPredict
from ..adapter_roberta import AdapterRobertaConfig, AdapterRobertaForMaskedLM
from ..bert import BertConfig, BertForMaskedLM, BertWithLengthPredict
from . import ConditionalMaskedLMConfig

logger = logging.get_logger(__name__)


class ConditionalMaskedLM(PreTrainedModel, AdapterModel):
    """
    Implementation of the `Conditional Masked Language Model`,
    introduced in the paper `Mask-Predict: Parallel Decoding of Conditional Masked Language Model`.
    The model will be instantiated with a :class:`AdapterBertWithLengthPredict` model as encoder
    and a :class:`AdapterBertForMaskedLM` model as decoder.
    Instantiate this class by either providing a configuration file or by providing an encoder and a decoder.
    """

    config_class = ConditionalMaskedLMConfig
    base_model_prefix = "conditional_masked_lm"

    def __init__(self, config=None, encoder=None, decoder=None):
        assert config is not None or (
            encoder is not None and decoder is not None
        ), "Either a configuration or an Encoder and a decoder has to be provided"

        if config is None:
            config = ConditionalMaskedLMConfig.from_encoder_decoder_configs(encoder.config, decoder.config)

        super().__init__(config)

        if encoder is None or decoder is None:
            if isinstance(config.encoder, AdapterBertConfig):
                encoder = AdapterBertWithLengthPredict(config.encoder)
            elif isinstance(config.encoder, AdapterAlbertConfig):
                encoder = AdapterAlbertWithLengthPredict(config.encoder)
            elif isinstance(config.encoder, BertConfig):
                encoder = BertWithLengthPredict(config.encoder)

            if isinstance(config.decoder, AdapterBertConfig):
                decoder = AdapterBertForMaskedLM(config.decoder)
            elif isinstance(config.decoder, AdapterRobertaConfig):
                decoder = AdapterRobertaForMaskedLM(config.decoder)
            elif isinstance(config.decoder, BertConfig):
                decoder = BertForMaskedLM(config.decoder)

        self.config = config
        self.encoder = encoder
        self.decoder = decoder

        if hasattr(self.decoder.config, "adapter_hidden_size"):
            self.encoder_to_decoder_adapter = nn.Linear(
                self.encoder.config.hidden_size, self.decoder.config.adapter_hidden_size
            )
        else:
            self.encoder_to_decoder_adapter = None

        self.length_loss_fct = nn.CrossEntropyLoss()

    @classmethod
    def from_pretrained(cls, *args, **kwargs):
        # At the moment fast initialization is not supported
        # for composite models
        kwargs["_fast_init"] = False
        return super().from_pretrained(*args, **kwargs)

    def forward(
        self,
        input_ids=None,
        attention_mask=None,
        decoder_input_ids=None,
        decoder_attention_mask=None,
        encoder_outputs=None,
        inputs_embeds=None,
        decoder_inputs_embeds=None,
        labels=None,
        target_lengths=None,
        return_dict=None,
        **kwargs
    ):
        """
        Args:
            target_lengths (:obj:`torch.LongTensor` of shape :obj:`batch_size`):
                The number of tokens in the target sequence.
                I.e. the number of tokens in lables (without padding tokens).
        """
        return_dict = return_dict if return_dict is not None else self.config.use_return_dict

        if encoder_outputs is None:
            encoder_outputs = self.encoder(
                input_ids=input_ids,
                attention_mask=attention_mask,
                inputs_embeds=inputs_embeds,
                return_dict=return_dict,
            )

        length_logits = encoder_outputs[2]
        encoder_hidden_states = encoder_outputs[0]
        if self.encoder_to_decoder_adapter is not None:
            encoder_hidden_states = self.encoder_to_decoder_adapter(encoder_hidden_states)

        # Decode
        if decoder_input_ids is not None or decoder_inputs_embeds is not None:
            decoder_outputs = self.decoder(
                input_ids=decoder_input_ids,
                attention_mask=decoder_attention_mask,
                encoder_hidden_states=encoder_hidden_states,
                encoder_attention_mask=attention_mask,
                inputs_embeds=decoder_inputs_embeds,
                labels=labels,
                return_dict=return_dict,
            )
        else:
            decoder_outputs = None

        if target_lengths is not None and labels is not None:
            length_loss = self.length_loss_fct(length_logits, target_lengths)
            loss = length_loss + decoder_outputs.loss
        else:
            length_loss = None
            loss = None

        if not return_dict:
            return (loss,) + encoder_outputs + decoder_outputs

        return ConditionalMaskedLMOutput(
            loss=loss,
            length_loss=length_loss,
            decoder_loss=decoder_outputs.loss if decoder_outputs is not None else None,
            length_logits=length_logits,
            decoder_logits=decoder_outputs.logits if decoder_outputs is not None else None,
            encoder_outputs=encoder_outputs,
        )

    def generate(
        self,
        input_ids,
        attention_mask,
        num_length_candidates,
        num_iterations,
        tgt_tokenizer,
        output_candidates=False,
        min_num_iterations=None,
    ):
        """
        Implementation of the parallel sequence decoding algorithm,
        as described in the paper `Mask-Predict: Parallel Decoding of Conditional Masked Language Model`.
        """

        if output_candidates and (min_num_iterations is None or min_num_iterations >= num_iterations):
            logger.error(
                "If `output_candidates` is set to True, then `min_num_iterations` must be set to a number smaller than `num_iterations`."
            )
            raise

        device = input_ids.device

        # Predict lengths
        forward_output = self.forward(input_ids, attention_mask, return_dict=True)
        encoder_outputs = forward_output.encoder_outputs
        length_logits = forward_output.length_logits

        lengths = torch.topk(length_logits, num_length_candidates, dim=1)[1]

        batch_size = input_ids.size(0)
        max_len = lengths.max().item() + 2  # we add [CLS] and [SEP] token to sequence

        # Create padding mask (length mask):
        # `False` if part of sequence, `True` if padding
        padding_mask = torch.triu(input_ids.new_ones((max_len, max_len), dtype=torch.bool))
        padding_mask = torch.stack(
            [padding_mask[(lengths[batch] - 1) + 2] for batch in range(batch_size)], dim=0
        )  # [batch_size x num_length_candidates x max_len]

        # Reshape
        reshaped_batch_size = batch_size * num_length_candidates
        padding_mask = padding_mask.reshape(reshaped_batch_size, max_len)
        lengths = lengths.view(reshaped_batch_size)

        # Tgt input_ids and token probabilities
        tgt_tokens = input_ids.new_full(padding_mask.size(), tgt_tokenizer.mask_token_id)
        tgt_probs = torch.zeros(padding_mask.size(), device=device)

        tgt_tokens[padding_mask] = tgt_tokenizer.pad_token_id
        tgt_probs[padding_mask] = 1.0

        tgt_tokens[:, 0] = tgt_tokenizer.cls_token_id
        tgt_probs[:, 0] = 1.0

        for i in range(reshaped_batch_size):
            tgt_tokens[i, lengths[i] + 1] = tgt_tokenizer.sep_token_id
            tgt_probs[i, lengths[i] + 1] = 1.0

        # Reshape encoder hidden-states
        # We only need to reshape hidden-states and predicted-lengths,
        # as this are the only attributes the forward method uses
        encoder_outputs.last_hidden_state = (
            encoder_outputs.last_hidden_state.unsqueeze(1)
            .repeat_interleave(num_length_candidates, dim=1)
            .reshape(reshaped_batch_size, -1, encoder_outputs.last_hidden_state.size(-1))
        )

        # Reshape encoder attention mask
        if attention_mask.dim() == 3:
            attention_mask = (
                attention_mask.unsqueeze(1)
                .repeat_interleave(num_length_candidates, dim=1)
                .reshape(reshaped_batch_size, -1, attention_mask.size(-1))
            )
        else:
            attention_mask = (
                attention_mask.unsqueeze(1)
                .repeat_interleave(num_length_candidates, dim=1)
                .reshape(reshaped_batch_size, -1)
            )

        # Mask predict
        candidate_tokens = []
        for i in range(num_iterations):
            # Create mask_mask: mask tokens with lowest probability
            # `True` if mask token, `False` else
            num_mask_tokens = (lengths.float() * (1.0 - (i / num_iterations))).long()

            mask_idxs = [
                tgt_probs[batch, :].topk(max(1, num_mask_tokens[batch].item()), largest=False, sorted=False)[1]
                for batch in range(reshaped_batch_size)
            ]
            mask_mask = tgt_tokens.new_zeros((tgt_tokens.size()), dtype=torch.bool)
            mask_mask = torch.stack(
                [mask_mask[batch].index_fill_(0, mask_idxs[batch], 1) for batch in range(reshaped_batch_size)]
            )

            # Predict masked tokens
            tgt_tokens.masked_fill_(mask_mask, tgt_tokenizer.mask_token_id)
            # Pass previous encoder_outputs instead of input_ids to speed up decoding
            tgt_logits = self.forward(
                encoder_outputs=encoder_outputs,
                attention_mask=attention_mask,
                decoder_input_ids=tgt_tokens,
                decoder_attention_mask=(~padding_mask).float(),
                return_dict=True,
            ).decoder_logits

            new_tgt_probs, new_tgt_tokens = F.softmax(tgt_logits, dim=-1).max(dim=-1)

            # Update tgt_tokens and tgt_probs
            tgt_tokens[mask_mask] = new_tgt_tokens[mask_mask]
            tgt_probs[mask_mask] = new_tgt_probs[mask_mask]

            if output_candidates and i >= min_num_iterations:
                candidate_tokens.append(tgt_tokens.clone())

        if output_candidates:
            candidate_tokens = torch.stack(candidate_tokens)
            # reshape to: # [batch_size x (candidates per len) x num_length_candidates x max_len]
            candidate_tokens = candidate_tokens.view(-1, batch_size, num_length_candidates, max_len).transpose(0, 1)
            # reshape to: # [batch_size x num_candidates x max_len]
            candidate_tokens = candidate_tokens.reshape(batch_size, -1, max_len)

            return candidate_tokens

        else:
            # Reshape back
            tgt_tokens = tgt_tokens.view(batch_size, num_length_candidates, -1)
            tgt_probs = tgt_probs.view(batch_size, num_length_candidates, -1)
            lengths = lengths.view(batch_size, num_length_candidates)

            # Keep best prediction
            log_probs = tgt_probs.log().sum(-1)
            avg_log_probs = log_probs / lengths.float()
            best_lengths = avg_log_probs.max(1)[1]

            tgt_tokens = torch.stack([tgt_tokens[batch, best_lengths[batch], :] for batch in range(batch_size)])
            return tgt_tokens


@dataclass()
class ConditionalMaskedLMOutput(Seq2SeqLMOutput):
    """
    Args:
        loss (:obj:`torch.FloatTensor` of shape :obj:`(1,)`, `optional`, returned when :obj:`labels` and :obj:`target_lengths` are provided):
            Language modeling loss (returns by the decoder) + length prediction loss (returned by the encoder).
            I.e. :obj:`decoder_loss` + :obj:`length_loss`
        length_loss (:obj:`torch.FloatTensor` of shape :obj:`(1,)`, `optional`, returned when :obj:`target_lengths` is provided):
            Length prediction loss.
        decoder_loss (:obj:`torch.FloatTensor` of shape :obj:`(1,)`, `optional`, returned when :obj:`labels` is provided):
            Language modeling loss.
        length_logits (:obj:`torch.FloatTensor` of shape :obj:`(batch_size, config.max_prediction_length)`):
            Prediction scores for the target length (scores for each length in [0, max_prediction_length] before SoftMax).
        decoder_logits (:obj:`torch.FloatTensor` of shape :obj:`(batch_size, sequence_length, config.vocab_size)`):
            Prediction scores of the language modeling head (scores for each vocabulary token before SoftMax).
        encoder_outputs:
            Output of self.encoder.
    """

    loss: Optional[torch.FloatTensor] = None
    length_loss: Optional[torch.FloatTensor] = None
    decoder_loss: Optional[torch.FloatTensor] = None
    length_logits: torch.FloatTensor = None
    decoder_logits: torch.FloatTensor = None
    encoder_outputs: Optional[Union[Tuple[torch.FloatTensor], ModelOutput]] = None
