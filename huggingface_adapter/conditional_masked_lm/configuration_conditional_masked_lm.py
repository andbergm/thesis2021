import copy

from transformers import PretrainedConfig

from ..adapter_albert import AdapterAlbertConfig
from ..adapter_bert import AdapterBertConfig
from ..adapter_roberta import AdapterRobertaConfig
from ..bert import BertConfig


class ConditionalMaskedLMConfig(PretrainedConfig):
    """
    Configuration class holds configuration for a :class:`AdapterBertWithLengthPredict` encoder
    and an :class:`AdapterBertForMaskedLM` decoder.
    """

    is_composition = True

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        encoder_config = kwargs.pop("encoder")
        encoder_model_type = encoder_config.pop("model_type").lower()

        decoder_config = kwargs.pop("decoder")
        decoder_model_type = decoder_config.pop("model_type").lower()

        if "adapter_bert" == encoder_model_type:
            self.encoder = AdapterBertConfig(**encoder_config)
        elif "adapter_albert" == encoder_model_type:
            self.encoder = AdapterAlbertConfig(**encoder_config)
        elif "bert" == encoder_model_type:
            self.encoder = BertConfig(**encoder_config)

        if "adapter_bert" == decoder_model_type:
            self.decoder = AdapterBertConfig(**decoder_config)
        elif "adapter_roberta" == decoder_model_type:
            self.decoder = AdapterRobertaConfig(**decoder_config)
        if "bert" == decoder_model_type:
            self.decoder = BertConfig(**decoder_config)

    @classmethod
    def from_encoder_decoder_configs(
        cls, encoder_config: PretrainedConfig, decoder_config: PretrainedConfig, **kwargs
    ) -> PretrainedConfig:

        return cls(encoder=encoder_config.to_dict(), decoder=decoder_config.to_dict(), **kwargs)

    def to_dict(self):
        """
        Serializes this instance to a Python dictionary. Override the default `to_dict()` from `PretrainedConfig`.

        Returns:
            :obj:`Dict[str, any]`: Dictionary of all the attributes that make up this configuration instance,
        """
        output = copy.deepcopy(self.__dict__)
        output["encoder"] = self.encoder.to_dict()
        output["decoder"] = self.decoder.to_dict()
        return output
