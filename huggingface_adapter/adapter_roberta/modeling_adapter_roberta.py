from transformers.models.roberta.modeling_roberta import (
    RobertaEmbeddings,
    RobertaForMaskedLM,
    RobertaLMHead,
    RobertaModel,
    RobertaPooler,
    RobertaPreTrainedModel,
)

from .. import AdapterModel
from ..adapter_bert.modeling_adapter_bert import AdapterBertEncoder


class AdapterRobertaModel(RobertaModel, AdapterModel):
    """
    Same as AdapterBertModel, but uses RobertaEmbeddings (instad of BertEmbeddings)
    """

    def __init__(self, config, add_pooling_layer=False):
        RobertaPreTrainedModel.__init__(self, config)

        self.config = config

        self.embeddings = RobertaEmbeddings(config)
        self.encoder = AdapterBertEncoder(config)  # Custom BertEncoder

        self.pooler = RobertaPooler(config) if add_pooling_layer else None

        self.init_weights()


class AdapterRobertaForMaskedLM(RobertaForMaskedLM, AdapterModel):
    """Adapter Roberta Model with a `language modeling` head on top."""

    def __init__(self, config):
        super().__init__(config)

        self.roberta = AdapterRobertaModel(config, add_pooling_layer=False)
        self.lm_head = RobertaLMHead(config)

        self.init_weights()
