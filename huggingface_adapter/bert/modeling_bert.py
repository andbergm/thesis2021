import torch
import torch.nn.functional as F
from torch import nn
from transformers.file_utils import ModelOutput
from transformers.modeling_utils import apply_chunking_to_forward
from transformers.models.bert.modeling_bert import (
    BertAttention,
    BertEmbeddings,
    BertEncoder,
    BertForMaskedLM,
    BertIntermediate,
    BertModel,
    BertOnlyMLMHead,
    BertOutput,
    BertPooler,
    BertPreTrainedModel,
)

"""
This module is a modification of the Huggingface Bert module to make it compatible with `Conditional Masked Language Modeling`.
This includes changing the Bert Layer to include cross attention layers and setting :obj:`is_decoder` to :obj:`False`. 
This allows it to be used as bidirectional decoder, i.e. without causal language modeling attention mask.
"""


class BertLayer(nn.Module):
    """
    Implementation copied from Huggingface `modeling_bert`.
    Modified s.t. `add_cross_attention` can be true while `is_decoder` is false.
    """

    def __init__(self, config):
        super().__init__()
        self.chunk_size_feed_forward = config.chunk_size_feed_forward
        self.seq_len_dim = 1
        self.attention = BertAttention(config)
        self.is_decoder = config.is_decoder
        self.add_cross_attention = config.add_cross_attention
        self.crossattention = BertAttention(config)
        self.intermediate = BertIntermediate(config)
        self.output = BertOutput(config)

    def forward(
        self,
        hidden_states,
        attention_mask=None,
        head_mask=None,
        encoder_hidden_states=None,
        encoder_attention_mask=None,
        past_key_value=None,
        output_attentions=False,
    ):
        # decoder uni-directional self-attention cached key/values tuple is at positions 1,2
        self_attn_past_key_value = past_key_value[:2] if past_key_value is not None else None
        self_attention_outputs = self.attention(
            hidden_states,
            attention_mask,
            head_mask,
            output_attentions=output_attentions,
            past_key_value=self_attn_past_key_value,
        )
        attention_output = self_attention_outputs[0]

        # if decoder, the last output is tuple of self-attn cache
        if self.is_decoder:
            outputs = self_attention_outputs[1:-1]
            present_key_value = self_attention_outputs[-1]
        else:
            outputs = self_attention_outputs[1:]  # add self attentions if we output attention weights

        cross_attn_present_key_value = None
        if encoder_hidden_states is not None:
            assert hasattr(
                self, "crossattention"
            ), f"If `encoder_hidden_states` are passed, {self} has to be instantiated with cross-attention layers by setting `config.add_cross_attention=True`"

            # cross_attn cached key/values tuple is at positions 3,4 of past_key_value tuple
            cross_attn_past_key_value = past_key_value[-2:] if past_key_value is not None else None
            cross_attention_outputs = self.crossattention(
                attention_output,
                attention_mask,
                head_mask,
                encoder_hidden_states,
                encoder_attention_mask,
                cross_attn_past_key_value,
                output_attentions,
            )
            attention_output = cross_attention_outputs[0]

            if self.is_decoder:
                outputs = (
                    outputs + cross_attention_outputs[1:-1]
                )  # add cross attentions if we output attention weights

                # add cross-attn cache to positions 3,4 of present_key_value tuple
                cross_attn_present_key_value = cross_attention_outputs[-1]
                present_key_value = present_key_value + cross_attn_present_key_value
            else:
                outputs = outputs + cross_attention_outputs[1:]  # add cross attentions if we output attention weights

        layer_output = apply_chunking_to_forward(
            self.feed_forward_chunk, self.chunk_size_feed_forward, self.seq_len_dim, attention_output
        )
        outputs = (layer_output,) + outputs

        # if decoder, return the attn key/values as the last output
        if self.is_decoder:
            outputs = outputs + (present_key_value,)

        return outputs

    def feed_forward_chunk(self, attention_output):
        intermediate_output = self.intermediate(attention_output)
        layer_output = self.output(intermediate_output, attention_output)
        return layer_output


class BertEncoder(BertEncoder):
    """
    Huggingface BertEncoder that uses the modified BertLayer.
    """

    def __init__(self, config):
        nn.Module.__init__(self)
        self.config = config
        self.layer = nn.ModuleList([BertLayer(config) for _ in range(config.num_hidden_layers)])


class BertModel(BertModel):
    """
    Huggingface BertModel that uses the modified BertEncoder.
    """

    def __init__(self, config, add_pooling_layer=True):
        BertPreTrainedModel.__init__(self, config)
        self.config = config

        self.embeddings = BertEmbeddings(config)
        self.encoder = BertEncoder(config)

        self.pooler = BertPooler(config) if add_pooling_layer else None

        self.init_weights()


class BertForMaskedLM(BertForMaskedLM):
    """
    Huggingface BertForMaskedLM that uses the modified BertModel.
    """

    def __init__(self, config):
        BertPreTrainedModel.__init__(self, config)

        self.bert = BertModel(config, add_pooling_layer=False)
        self.cls = BertOnlyMLMHead(config)

        self.init_weights()


class BertWithLengthPredict(BertModel):
    """
    Modification of the Bert Model, with length prediction.
    The maximum length has to be specified in the :obj:`max_prediction_length` attribute of the configuration.
    This model is not intended to be used as decoder! I.e. the arguments :obj:`is_decoder`
    in the provided configuration should be set to :obj:`False`.
    """

    def __init__(self, config):
        super().__init__(config)

        self.length_embeddings = nn.Embedding(config.max_prediction_length +1, config.hidden_size) # +1, as length 0 is not predicted

    def forward(
        self,
        input_ids=None,
        attention_mask=None,
        token_type_ids=None,
        position_ids=None,
        head_mask=None,
        inputs_embeds=None,
        return_dict=None,
    ):
        """
        Modified Bert forward method.
        A special length token is added to the input sequence.
        The output hidden state of this token is then used to decode the length.

        Return:
        :obj:`tuple(torch.FloatTensor)` comprising the following elements:
        last_hidden_state (:obj:`torch.FloatTensor` of shape :obj:`(batch_size, sequence_length, hidden_size)`):
            Sequence of hidden-states at the output of the last layer of the model.
            N.B. The hidden state of the internally added special length is not contained in this output sequence.
        pooler_output (:obj:`torch.FloatTensor`: of shape :obj:`(batch_size, hidden_size)`):
            Last layer hidden-state of the first token of the sequence (classification token)
            further processed by a Linear layer and a Tanh activation function. The Linear
            layer weights are trained from the next sentence prediction (classification)
            objective during pre-training.
        predicted_lengths (:obj:`torch.FloatTensor`: of shape :obj:`(batch_size, max_prediction_length)`):
            Decoded last layer hidden-state of the added length token.
        """
        return_dict = return_dict if return_dict is not None else self.config.use_return_dict

        if input_ids is not None and inputs_embeds is not None:
            raise ValueError("You cannot specify both input_ids and inputs_embeds at the same time")
        elif input_ids is not None:
            input_shape = input_ids.size()
            batch_size, seq_length = input_shape
        elif inputs_embeds is not None:
            input_shape = inputs_embeds.size()[:-1]
            batch_size, seq_length = input_shape
        else:
            raise ValueError("You have to specify either input_ids or inputs_embeds")

        device = input_ids.device if input_ids is not None else inputs_embeds.device

        if attention_mask is None:
            attention_mask = torch.ones(((batch_size, seq_length)), device=device)
        if token_type_ids is None:
            token_type_ids = torch.zeros(input_shape, dtype=torch.long, device=device)

        # Extend attention mask for length token
        if attention_mask.dim() == 3:
            attention_mask = torch.cat(
                [attention_mask.new_ones((batch_size, 1, attention_mask.size(2))), attention_mask], dim=1
            )
        else:
            attention_mask = torch.cat([attention_mask.new_ones((batch_size, 1)), attention_mask], dim=1)

        # We can provide a self-attention mask of dimensions [batch_size, from_seq_length, to_seq_length]
        # ourselves in which case we just need to make it broadcastable to all heads.
        extended_attention_mask: torch.Tensor = self.get_extended_attention_mask(attention_mask, input_shape, device)

        # Prepare head mask if needed
        # 1.0 in head_mask indicate we keep the head
        # attention_probs has shape bsz x n_heads x N x N
        # input head_mask has shape [num_heads] or [num_hidden_layers x num_heads]
        # and head_mask is converted to shape [num_hidden_layers x batch x num_heads x seq_length x seq_length]
        head_mask = self.get_head_mask(head_mask, self.config.num_hidden_layers)

        embedding_output = self.embeddings(
            input_ids=input_ids,
            position_ids=position_ids,
            token_type_ids=token_type_ids,
            inputs_embeds=inputs_embeds,
        )

        # Length tokens
        length_tokens = torch.zeros((batch_size, 1), dtype=torch.long, device=device)
        length_embedding_output = self.length_embeddings(length_tokens)
        embedding_output = torch.cat([length_embedding_output, embedding_output], dim=1)

        encoder_outputs = self.encoder(
            embedding_output,
            attention_mask=extended_attention_mask,
            head_mask=head_mask,
            return_dict=return_dict,
        )
        sequence_output = encoder_outputs[0]

        # remove length from sequence output
        length_hidden_state = sequence_output[:, 0, :]
        last_hidden_state = sequence_output[:, 1:, :]

        pooled_output = self.pooler(last_hidden_state) if self.pooler is not None else None

        # predict lengths
        predicted_lengths_logits = torch.matmul(
            length_hidden_state, self.length_embeddings.weight.transpose(0, 1)
        ).float()
        predicted_lengths_logits[:, 0] += float("-inf")  # Cannot predict the len_token
        predicted_lengths = F.log_softmax(predicted_lengths_logits, dim=-1)

        if not return_dict:
            return last_hidden_state, pooled_output, predicted_lengths

        return BertWithLengthPreditOutput(
            last_hidden_state=last_hidden_state, pooler_output=pooled_output, predicted_lengths=predicted_lengths
        )


class BertWithLengthPreditOutput(ModelOutput):
    """
    Base class for model's outputs that also contains a pooling of the last hidden states.

    Args:
        last_hidden_state (:obj:`torch.FloatTensor` of shape :obj:`(batch_size, sequence_length, hidden_size)`):
            Sequence of hidden-states at the output of the last layer of the model.
            N.B. The hidden state of the internally added special length is not contained in this output sequence.
        pooler_output (:obj:`torch.FloatTensor`: of shape :obj:`(batch_size, hidden_size)`):
            Last layer hidden-state of the first token of the sequence (classification token)
            further processed by a Linear layer and a Tanh activation function. The Linear
            layer weights are trained from the next sentence prediction (classification)
            objective during pre-training.
        predicted_lengths (:obj:`torch.FloatTensor`: of shape :obj:`(batch_size, max_prediction_length)`):
            Decoded last layer hidden-state of the added length token.
    """

    last_hidden_state: torch.FloatTensor = None
    pooler_output: torch.FloatTensor = None
    predicted_lengths: torch.FloatTensor = None
