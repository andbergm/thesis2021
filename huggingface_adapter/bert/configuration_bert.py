from transformers import BertConfig


class AdapterBertConfig(BertConfig):
    """
    Extention of the Huggingface :class:`BertConfig` class with added parameter for length prediction.

    See :class:`BertConfig` for documentation.

    Args:
        max_prediction_length (:obj:`int`):
            The size of the output vocabulary of the length token.
            Only relevant for the class :class:`AdapterBertWithLengthPredict`.
    """

    def __init__(self, max_prediction_length=128, **kwargs):
        super().__init__(**kwargs)
        self.max_prediction_length = max_prediction_length
