import json
import os
import pickle

import numpy as np
import selfies
import torch
from rdkit import Chem
from rdkit.Chem import Descriptors
from rdkit.ML.Descriptors.MoleculeDescriptors import MolecularDescriptorCalculator
from scipy import stats
from torch.utils.data import Dataset
from multiprocessing import Pool
import multiprocessing


class PychemFeatures:
    def __init__(self):
        distributions_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), "physchem_distributions.json")
        with open(distributions_path) as fp:
            distributions = json.load(fp)

        self.descriptors = sorted(distributions.keys())
        self.descriptor_calc = MolecularDescriptorCalculator(self.descriptors)

        # Get Cumulative distribution function for all descriptors
        self.cdfs = {}
        for descriptor_name, (dist, params, minV, maxV, avg, std) in distributions.items():
            arg = params[:-2]  # type: ignore
            loc = params[-2]  # type: ignore
            scale = params[-1]  # type: ignore

            dist = getattr(stats, dist)

            # make the cdf with the parameters
            def cdf(v, dist=dist, arg=arg, loc=loc, scale=scale, minV=minV, maxV=maxV):
                v = dist.cdf(np.clip(v, minV, maxV), loc=loc, scale=scale, *arg)
                return np.clip(v, 0.0, 1.0)

            self.cdfs[descriptor_name] = cdf

    def __call__(self, mol):
        x = self.descriptor_calc.CalcDescriptors(mol)
        x = np.array(x, dtype=np.float32)
        x[~np.isfinite(x)] = 0

        # normalize descriptors
        x = [self.cdfs[descriptor](x[i]) for i, descriptor in enumerate(self.descriptors)]

        return np.array(x, dtype=np.float32)


class SelfiesDataset(Dataset):
    """
    A pytorch dataset class, that takes a list of SMILES strings, transformes them into SELFIES string
    and stores them alongside with their normalized molecular descriptors.
    """

    def __init__(self, smiles=None, canonicalize=True, max_len=-1, dataset=None):
        super().__init__()

        if dataset is not None:
            self.__load_dataset(dataset)
            return

        if canonicalize:
            smiles = list(map(self.__canonicalize_smiles, smiles))

        selfies_strings = list(map(selfies.encoder, smiles))

        pychemFeatures = PychemFeatures()
        descriptors = [pychemFeatures(Chem.MolFromSmiles(s)) for s in smiles]
        # Len filter
        if max_len >= 0:
            self.selfies_strings = []
            self.descriptors = []
            for s, d in zip(selfies_strings, descriptors):
                if selfies.len_selfies(s) <= max_len:
                    self.selfies_strings.append(s)
                    self.descriptors.append(d)
        else:
            self.selfies_strings = selfies_strings
            self.descriptors = descriptors

        self.len = len(self.selfies_strings)

    def __len__(self):
        return self.len

    def __getitem__(self, idx):
        return self.selfies_strings[idx], self.descriptors[idx]

    def __canonicalize_smiles(self, smiles):
        mol = Chem.MolFromSmiles(smiles, sanitize=False)

        flags = Chem.SanitizeFlags.SANITIZE_ALL ^ Chem.SanitizeFlags.SANITIZE_CLEANUP
        Chem.SanitizeMol(mol, flags, catchErrors=True)

        # bug where permuted smiles are not canonicalised to the same form. This is fixed by round tripping SMILES
        mol = Chem.MolFromSmiles(Chem.MolToSmiles(mol))

        Chem.Kekulize(mol, clearAromaticFlags=True)
        smiles = Chem.MolToSmiles(mol, kekuleSmiles=True, canonical=True)
        return smiles

    def save_dataset(self, file):
        dataset = self.selfies_strings, self.descriptors
        with open(file, "wb") as f:
            pickle.dump(dataset, f)

    def __load_dataset(self, file):
        with open(file, "rb") as f:
            dataset = pickle.load(f)

        self.selfies_strings, self.descriptors = dataset
        self.len = len(self.selfies_strings)


class SelfiesDataCollator:
    def __init__(self, tokenizer, max_length, mlm_probability=0.15):
        self.tokenizer = tokenizer
        self.max_length = max_length
        self.mlm_probability = mlm_probability

    def __call__(self, batch):
        selfies, descriptors = map(list, zip(*batch))

        output = self.tokenizer(
            selfies,
            return_tensors="pt",
            return_attention_mask=True,
            return_special_tokens_mask=True,
            padding="max_length",
            truncation=True,
            max_length=self.max_length,
        )
        special_tokens_mask = output.pop("special_tokens_mask", None)
        output["input_ids"], output["labels"] = self.mask_tokens(output["input_ids"], special_tokens_mask)
        output["physchem_labels"] = torch.tensor(descriptors, dtype=torch.float)

        return output

    def mask_tokens(self, input_ids, special_tokens_mask):
        labels = input_ids.clone()

        probability_matrix = torch.full(labels.shape, self.mlm_probability)
        probability_matrix[special_tokens_mask] = 0.0
        masked_indices = torch.bernoulli(probability_matrix).bool()
        labels[~masked_indices] = -100  # We only compute loss on masked tokens

        # 80% of the time, we replace masked input tokens with tokenizer.mask_token ([MASK])
        indices_replaced = torch.bernoulli(torch.full(labels.shape, 0.8)).bool() & masked_indices
        input_ids[indices_replaced] = self.tokenizer.mask_token_id

        # 10% of the time, we replace masked input tokens with random word
        indices_random = torch.bernoulli(torch.full(labels.shape, 0.5)).bool() & masked_indices & ~indices_replaced
        random_words = torch.randint(len(self.tokenizer), labels.shape, dtype=torch.long)
        input_ids[indices_random] = random_words[indices_random]

        # The rest of the time (10% of the time) we keep the masked input tokens unchanged
        return input_ids, labels
