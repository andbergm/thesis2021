from dataclasses import dataclass
from typing import Optional, Tuple

import torch
from torch import nn
from transformers.file_utils import ModelOutput
from transformers.models.bert.modeling_bert import BertLMPredictionHead, BertModel, BertPreTrainedModel


class PhysChemHead(nn.Module):
    def __init__(self, config):
        super().__init__()
        self.physchem_clf = nn.Sequential(
            nn.Dropout(config.hidden_dropout_prob),
            nn.Linear(config.hidden_size, config.hidden_size),
            nn.ReLU(),
            nn.Linear(config.hidden_size, 200),  # we predict 200 chemical descriptors
        )

    def forward(self, pooled_output):
        return self.physchem_clf(pooled_output)


class SelfiesBertHeads(nn.Module):
    def __init__(self, config):
        super().__init__()
        self.lm_head = BertLMPredictionHead(config)
        self.physchem_head = PhysChemHead(config)

    def forward(self, sequence_output, pooled_output):
        lm_scores = self.lm_head(sequence_output)
        physchem_scores = self.physchem_head(pooled_output)
        return lm_scores, physchem_scores


@dataclass
class SelfiesBERTOutput(ModelOutput):
    loss: Optional[torch.FloatTensor] = None
    lm_logits: torch.FloatTensor = None
    physchem_logits: torch.FloatTensor = None
    hidden_states: Optional[Tuple[torch.FloatTensor]] = None
    attentions: Optional[Tuple[torch.FloatTensor]] = None


class SelfiesBERT(BertPreTrainedModel):
    def __init__(self, config):
        super().__init__(config)

        self.bert = BertModel(config)
        self.cls = SelfiesBertHeads(config)

        self.init_weights()

    def forward(
        self,
        input_ids=None,
        attention_mask=None,
        token_type_ids=None,
        position_ids=None,
        head_mask=None,
        inputs_embeds=None,
        labels=None,
        physchem_labels=None,
        output_attentions=None,
        output_hidden_states=None,
        return_dict=None,
    ):

        return_dict = return_dict if return_dict is not None else self.config.use_return_dict

        outputs = self.bert(
            input_ids,
            attention_mask=attention_mask,
            token_type_ids=token_type_ids,
            position_ids=position_ids,
            head_mask=head_mask,
            inputs_embeds=inputs_embeds,
            output_attentions=output_attentions,
            output_hidden_states=output_hidden_states,
            return_dict=return_dict,
        )

        sequence_output, pooled_output = outputs[:2]
        lm_scores, physchem_scores = self.cls(sequence_output, pooled_output)

        total_loss = None
        if labels is not None and physchem_labels is not None:
            lm_loss_fct = nn.CrossEntropyLoss()
            masked_lm_loss = lm_loss_fct(lm_scores.view(-1, self.config.vocab_size), labels.view(-1))
            physchem_loss_fct = nn.MSELoss()
            physchem_loss = physchem_loss_fct(physchem_scores, physchem_labels)
            total_loss = masked_lm_loss + physchem_loss

        if not return_dict:
            output = (lm_scores, physchem_scores) + outputs[2:]
            return ((total_loss,) + output) if total_loss is not None else output

        return SelfiesBERTOutput(
            loss=total_loss,
            lm_logits=lm_scores,
            physchem_logits=physchem_scores,
            hidden_states=outputs.hidden_states,
            attentions=outputs.attentions,
        )
