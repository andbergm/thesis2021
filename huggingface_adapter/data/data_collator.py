import torch
from transformers.utils import logging

logger = logging.get_logger(__name__)


class DataCollatorForConditionalMaskedLM:
    """
    Data collator used for conditional masked language modeling.
    A batch is expected to contain a tuple of raw (not tokenized) strings for the source and target sequence.
    The provided source- and target-tokenizers are used to decode the sequences respectively.
    Inputs are dynamically padded to the maximum length of a batch if they are not all of the same length.
    Non-special tokens in the target sequence are masked with probability `1 / sequence-length`.
    Tokens to be masked are replaced (100% of the time) with :obj:`tokenizer.mask_token`.
    Args:
        src_tokenizer (:class:`~transformers.PreTrainedTokenizer` or :class:`~transformers.PreTrainedTokenizerFast`):
            The tokenizer used for encoding the source string.
        tgt_tokenizer (:class:`~transformers.PreTrainedTokenizer` or :class:`~transformers.PreTrainedTokenizerFast`):
            The tokenizer used for encoding the target string.
        src_tokenizer_kwargs (`optional`):
            Kwargs passed to the src_tokenizer.
        tgt_tokenizer_kwargs (`optional`):
            Kwargs passed to the tgt_tokenizer.
    """

    def __init__(self, src_tokenizer, tgt_tokenizer, src_tokenizer_kwargs, tgt_tokenizer_kwargs):
        self.src_tokenizer = src_tokenizer
        self.tgt_tokenizer = tgt_tokenizer
        self.src_tokenizer_kwargs = src_tokenizer_kwargs
        self.tgt_tokenizer_kwargs = tgt_tokenizer_kwargs

    def __call__(self, batch):
        src, tgt = map(list, zip(*batch))

        # Tokenize
        src = self.src_tokenizer(
            src,
            return_tensors="pt",
            return_attention_mask=True,
            **self.src_tokenizer_kwargs,
        )
        tgt = self.tgt_tokenizer(
            tgt,
            return_tensors="pt",
            return_attention_mask=True,
            return_special_tokens_mask=True,
            **self.tgt_tokenizer_kwargs,
        )
        labels = tgt.input_ids.clone()

        # Target lengths
        length_mask = tgt.special_tokens_mask.bool()
        tgt_lengths = (~length_mask).long().sum(-1)

        # Masking
        num_mask_tokens = torch.rand_like(tgt_lengths, dtype=torch.float) * tgt_lengths
        mask_probs = num_mask_tokens.float() / tgt_lengths.float()

        mask_probs = mask_probs.unsqueeze(1).repeat_interleave(tgt.input_ids.size(1), dim=1)

        mask_probs[length_mask] = 0.0
        mask_mask = torch.bernoulli(mask_probs).bool()

        tgt.input_ids[mask_mask] = self.tgt_tokenizer.mask_token_id
        labels[~mask_mask] = -100  # We only compute loss on masked tokens

        return {
            "input_ids": src.input_ids,
            "attention_mask": src.attention_mask,
            "decoder_input_ids": tgt.input_ids,
            "decoder_attention_mask": tgt.attention_mask,
            "labels": labels,
            "target_lengths": tgt_lengths,
        }
