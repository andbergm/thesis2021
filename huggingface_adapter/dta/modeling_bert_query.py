import torch
from torch import nn
import math


class Attention(nn.Module):
    def __init__(self, config):
        super().__init__()
        if config.hidden_size % config.num_attention_heads != 0 and not hasattr(config, "embedding_size"):
            raise ValueError(
                f"The hidden size ({config.hidden_size}) is not a multiple of the number of attention "
                f"heads ({config.num_attention_heads})"
            )

        self.num_attention_heads = config.num_attention_heads
        self.attention_head_size = int(config.hidden_size / config.num_attention_heads)
        self.all_head_size = self.num_attention_heads * self.attention_head_size

        self.dropout = nn.Dropout(config.attention_probs_dropout_prob)

    def transpose_for_scores(self, x):
        new_x_shape = x.size()[:-1] + (self.num_attention_heads, self.attention_head_size)
        x = x.view(*new_x_shape)
        return x.permute(0, 2, 1, 3)

    def forward(
        self,
        query,
        past_key_value,
        attention_mask=None,
        head_mask=None,
    ):
        key_layer = past_key_value[0]
        value_layer = past_key_value[1]

        query = query.unsqueeze(0)  # add sequence dimension
        query = query.unsqueeze(0).repeat_interleave(key_layer.size(0), 0)  # copy for all elements in batch

        query_layer = self.transpose_for_scores(query)

        # Take the dot product between "query" and "key" to get the raw attention scores.
        attention_scores = torch.matmul(query_layer, key_layer.transpose(-1, -2))

        attention_scores = attention_scores / math.sqrt(self.attention_head_size)
        if attention_mask is not None:
            # Apply the attention mask is (precomputed for all layers in BertModel forward() function)
            attention_scores = attention_scores + attention_mask

        # Normalize the attention scores to probabilities.
        attention_probs = nn.Softmax(dim=-1)(attention_scores)

        # This is actually dropping out entire tokens to attend to, which might
        # seem a bit unusual, but is taken from the original Transformer paper.
        attention_probs = self.dropout(attention_probs)

        # Mask heads if we want to
        if head_mask is not None:
            attention_probs = attention_probs * head_mask

        context_layer = torch.matmul(attention_probs, value_layer)

        context_layer = context_layer.permute(0, 2, 1, 3).contiguous()
        new_context_layer_shape = context_layer.size()[:-2] + (self.all_head_size,)
        context_layer = context_layer.view(*new_context_layer_shape)

        context_layer = context_layer.squeeze(1)  # remove sequence dimension

        return context_layer


class BertQueryLayer(nn.Module):
    def __init__(self, config):
        super().__init__()

        self.query = nn.parameter.Parameter(torch.empty(config.hidden_size))
        self.attention = Attention(config)
        self.layerNorm = nn.LayerNorm(config.hidden_size, eps=config.layer_norm_eps)
        self.reset_parameters()

    def reset_parameters(self):
        nn.init.normal_(self.query)

    def forward(
        self,
        past_key_value,
        attention_mask=None,
        head_mask=None,
    ):
        output = self.attention(self.query, past_key_value, attention_mask, head_mask)
        output = self.layerNorm(output)
        return output


class BertQueryModel(nn.Module):
    def __init__(self, config, num_query_layers):
        super().__init__()

        self.config = config
        self.num_query_layers = num_query_layers
        self.query_layers = nn.ModuleList([BertQueryLayer(config) for _ in range(num_query_layers)])

    def forward(self, past_key_values):
        # we fuse the top layers of past_key_values
        num_layers = len(past_key_values)
        past_key_values = [past_key_values[i] for i in range(num_layers - self.num_query_layers, num_layers)]
        queried_layers = [self.query_layers[i](past_key_values[i]) for i in range(self.num_query_layers)]

        queried_model = torch.stack(queried_layers).sum(0)

        return queried_model
