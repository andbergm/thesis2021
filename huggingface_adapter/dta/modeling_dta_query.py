from dataclasses import dataclass
from typing import Optional

import torch
import torch.nn.functional as F
from torch import nn
from transformers import BertConfig, PreTrainedModel, RobertaConfig
from transformers.file_utils import ModelOutput

from ..adapter_albert import AdapterAlbertConfig, AdapterAlbertModel
from ..adapter_bert import AdapterBertConfig, AdapterBertModel
from ..adapter_roberta import AdapterRobertaConfig, AdapterRobertaModel
from .configuration_dta import DTAConfig
from .modeling_bert import BertModel
from .modeling_bert_query import BertQueryModel
from .modeling_roberta import RobertaModel


class DTAModel(PreTrainedModel):

    config_class = DTAConfig
    base_model_prefix = "dta"

    def __init__(self, config=None, prot_encoder=None, lig_encoder=None, **kwargs):
        assert config is not None or (
            prot_encoder is not None and lig_encoder is not None
        ), "Either a configuration or an encoder for the proteins and ligands has to be provided"

        if config is None:
            config = DTAConfig.from_encoder_configs(prot_encoder.config, lig_encoder.config, **kwargs)

        super().__init__(config)

        if prot_encoder is None or lig_encoder is None:
            prot_encoder = BertModel(config.prot_encoder)
            lig_encoder = RobertaModel(config.lig_encoder)

        self.config = config
        self.prot_encoder = prot_encoder
        self.lig_encoder = lig_encoder

        self.prot_query_model = BertQueryModel(config.prot_encoder, num_query_layers=6)
        self.lig_query_model = BertQueryModel(config.lig_encoder, num_query_layers=4)

        self.regressor = Regressor(config)
        self.mse_loss = nn.MSELoss()

    def forward(
        self,
        prot_input_ids,
        prot_attention_mask,
        lig_input_ids,
        lig_attention_mask,
        labels=None,
        return_dict=None,
    ):
        return_dict = return_dict if return_dict is not None else self.config.use_return_dict

        prot_key_values = self.prot_encoder(prot_input_ids, prot_attention_mask).past_key_values
        prot_query_output = self.prot_query_model(prot_key_values)

        lig_key_values = self.lig_encoder(lig_input_ids, lig_attention_mask).past_key_values
        lig_query_output = self.lig_query_model(lig_key_values)

        predicted_binding_scores = self.regressor(prot_query_output, lig_query_output).squeeze(1)

        if labels is not None:
            loss = self.mse_loss(predicted_binding_scores, labels)
        else:
            loss = None

        if not return_dict:
            return (loss, predicted_binding_scores)

        return DTAOutput(loss, predicted_binding_scores)

    @classmethod
    def from_pretrained(cls, *args, **kwargs):
        # At the moment fast initialization is not supported
        # for composite models
        kwargs["_fast_init"] = False
        return super().from_pretrained(*args, **kwargs)


@dataclass()
class DTAOutput(ModelOutput):
    loss: Optional[torch.FloatTensor] = None
    predicted_binding_score: Optional[torch.FloatTensor] = None


class Regressor(nn.Module):
    def __init__(self, config):
        super().__init__()

        hidden_size = config.prot_encoder.hidden_size + config.lig_encoder.hidden_size
        self.layerNorm = nn.LayerNorm(hidden_size)
        self.l1 = nn.Linear(
            hidden_size,
            config.regressor_hidden_size,
        )
        self.l2 = nn.Linear(config.regressor_hidden_size, config.regressor_hidden_size)
        self.l3 = nn.Linear(config.regressor_hidden_size, 1)
        self.relu = nn.ReLU()

    def forward(self, prot_embeddings, lig_embeddings):
        x = torch.cat([prot_embeddings, lig_embeddings], dim=1)
        x = self.layerNorm(x)
        x = self.l1(x)
        x = self.relu(x)
        x = self.l2(x)
        x = self.relu(x)
        y = self.l3(x)

        return y
