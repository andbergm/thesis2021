from dataclasses import dataclass
from typing import Optional

import torch
import torch.nn.functional as F
from torch import nn
from transformers import BertConfig, BertModel, PreTrainedModel, RobertaConfig, RobertaModel
from transformers.file_utils import ModelOutput

from ..adapter_albert import AdapterAlbertConfig, AdapterAlbertModel
from ..adapter_bert import AdapterBertConfig, AdapterBertModel
from ..adapter_roberta import AdapterRobertaConfig, AdapterRobertaModel
from .configuration_dta import DTAConfig


class DTAModel(PreTrainedModel):

    config_class = DTAConfig
    base_model_prefix = "dta"

    def __init__(self, config=None, prot_encoder=None, lig_encoder=None, **kwargs):
        assert config is not None or (
            prot_encoder is not None and lig_encoder is not None
        ), "Either a configuration or an encoder for the proteins and ligands has to be provided"

        if config is None:
            config = DTAConfig.from_encoder_configs(prot_encoder.config, lig_encoder.config, **kwargs)

        super().__init__(config)

        if prot_encoder is None or lig_encoder is None:
            if isinstance(config.prot_encoder, AdapterBertConfig):
                prot_encoder = AdapterBertModel(config.prot_encoder)
            elif isinstance(config.prot_encoder, BertConfig):
                prot_encoder = BertModel(config.prot_encoder)
            elif isinstance(config.prot_encoder, RobertaConfig):
                prot_encoder = RobertaModel(config.prot_encoder)

            if isinstance(config.lig_encoder, AdapterBertConfig):
                lig_encoder = AdapterBertModel(config.lig_encoder)
            elif isinstance(config.lig_encoder, AdapterRobertaConfig):
                lig_encoder = AdapterRobertaModel(config.lig_encoder)
            elif isinstance(config.lig_encoder, BertConfig):
                lig_encoder = BertModel(config.lig_encoder)
            elif isinstance(config.lig_encoder, RobertaConfig):
                lig_encoder = RobertaModel(config.lig_encoder)

        self.config = config
        self.prot_encoder = prot_encoder
        self.lig_encoder = lig_encoder

        self.regressor = SmallRegressor(config)
        self.mse_loss = nn.MSELoss()

    def forward(
        self,
        prot_input_ids,
        prot_attention_mask,
        lig_input_ids,
        lig_attention_mask,
        labels=None,
        prot_special_tokens_mask=None,
        lig_special_tokens_mask=None,
        return_dict=None,
    ):
        return_dict = return_dict if return_dict is not None else self.config.use_return_dict

        if self.config.mean_pool_proteins:
            if prot_special_tokens_mask is None:
                raise ValueError("The `special tokens mask` for proteins has to be provided")

            prot_embeddings = self.mean_pool(
                self.prot_encoder(prot_input_ids, prot_attention_mask)[0], prot_special_tokens_mask
            )
        else:
            prot_embeddings = self.prot_encoder(prot_input_ids, prot_attention_mask)[0][:, 0]

        if self.config.mean_pool_ligands:
            if lig_special_tokens_mask is None:
                raise ValueError("The `special tokens mask` for ligands has to be provided")

            lig_embeddings = self.mean_pool(
                self.lig_encoder(lig_input_ids, lig_attention_mask)[0], lig_special_tokens_mask
            )
        else:
            lig_embeddings = self.lig_encoder(lig_input_ids, lig_attention_mask)[0][:, 0]

        predicted_binding_scores = self.regressor(prot_embeddings, lig_embeddings).squeeze(1)

        if labels is not None:
            loss = self.mse_loss(predicted_binding_scores, labels)
        else:
            loss = None

        if not return_dict:
            return (loss, predicted_binding_scores)

        return DTAOutput(loss, predicted_binding_scores)

    def mean_pool(self, embeddings, special_tokens_mask):
        special_tokens_mask = (1 - special_tokens_mask).float()  # 0 if special token (pad, cls, sep), 1 else
        special_tokens_mask = special_tokens_mask.unsqueeze(-1)  # [B x S x 1]
        embeddings = embeddings * special_tokens_mask

        seq_lengths = torch.sum(special_tokens_mask, dim=1)
        embeddings = torch.sum(embeddings, dim=1) / seq_lengths  # mean along length-dimension
        return embeddings

    @classmethod
    def from_pretrained(cls, *args, **kwargs):
        # At the moment fast initialization is not supported
        # for composite models
        kwargs["_fast_init"] = False
        return super().from_pretrained(*args, **kwargs)


@dataclass()
class DTAOutput(ModelOutput):
    loss: Optional[torch.FloatTensor] = None
    predicted_binding_score: Optional[torch.FloatTensor] = None


class Regressor(nn.Module):
    def __init__(self, config):
        super().__init__()

        combined_size = config.prot_encoder.hidden_size + config.lig_encoder.hidden_size

        self.regressor = nn.Sequential(
            nn.LayerNorm(combined_size),
            nn.Linear(combined_size, 1024),
            nn.ReLU(),
            nn.Dropout(0.1),
            nn.Linear(1024, 1024),
            nn.ReLU(),
            nn.Dropout(0.1),
            nn.Linear(1024, 512),
            nn.ReLU(),
            nn.Linear(512, 1),
        )

    def forward(self, prot_embeddings, lig_embeddings):
        x = torch.cat([prot_embeddings, lig_embeddings], dim=1)
        return self.regressor(x)


class SmallRegressor(nn.Module):
    def __init__(self, config):
        super().__init__()

        combined_size = config.prot_encoder.hidden_size + config.lig_encoder.hidden_size
        hidden_size = config.regressor_hidden_size

        self.regressor = nn.Sequential(
            nn.LayerNorm(combined_size),
            nn.Linear(combined_size, hidden_size),
            nn.ReLU(),
            nn.Linear(hidden_size, hidden_size),
            nn.ReLU(),
            nn.Linear(hidden_size, 1),
        )

    def forward(self, prot_embeddings, lig_embeddings):
        x = torch.cat([prot_embeddings, lig_embeddings], dim=1)
        return self.regressor(x)
