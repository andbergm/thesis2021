import copy

from transformers import BertConfig, PretrainedConfig, RobertaConfig

from ..adapter_albert import AdapterAlbertConfig
from ..adapter_bert import AdapterBertConfig
from ..adapter_roberta import AdapterRobertaConfig


class DTAConfig(PretrainedConfig):

    is_composition = True

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        prot_encoder_config = kwargs.pop("prot_encoder")
        prot_encoder_model_type = prot_encoder_config.pop("model_type").lower()

        lig_encoder_config = kwargs.pop("lig_encoder")
        lig_encoder_model_type = lig_encoder_config.pop("model_type").lower()

        if "adapter_bert" == prot_encoder_model_type:
            self.prot_encoder = AdapterBertConfig(**prot_encoder_config)
        elif "adapter_albert" == prot_encoder_model_type:
            self.prot_encoder = AdapterAlbertConfig(**prot_encoder_config)
        elif "bert" == prot_encoder_model_type:
            self.prot_encoder = BertConfig(**prot_encoder_config)
        elif "roberta" == prot_encoder_model_type:
            self.prot_encoder = RobertaConfig(**prot_encoder_config)

        if "adapter_bert" == lig_encoder_model_type:
            self.lig_encoder = AdapterBertConfig(**lig_encoder_config)
        elif "adapter_roberta" == lig_encoder_model_type:
            self.lig_encoder = AdapterRobertaConfig(**lig_encoder_config)
        elif "bert" == lig_encoder_model_type:
            self.lig_encoder = BertConfig(**lig_encoder_config)
        elif "roberta" == lig_encoder_model_type:
            self.lig_encoder = RobertaConfig(**lig_encoder_config)

        self.regressor_hidden_size = kwargs.pop("regressor_hidden_size", 128)
        self.mean_pool_proteins = kwargs.pop("mean_pool_proteins", False)
        self.mean_pool_ligands = kwargs.pop("mean_pool_ligands", False)


    @classmethod
    def from_encoder_configs(
        cls, prot_encoder_config: PretrainedConfig, lig_encoder_config: PretrainedConfig, **kwargs
    ) -> PretrainedConfig:

        return cls(prot_encoder=prot_encoder_config.to_dict(), lig_encoder=lig_encoder_config.to_dict(), **kwargs)

    def to_dict(self):
        """
        Serializes this instance to a Python dictionary. Override the default `to_dict()` from `PretrainedConfig`.

        Returns:
            :obj:`Dict[str, any]`: Dictionary of all the attributes that make up this configuration instance,
        """
        output = copy.deepcopy(self.__dict__)
        output["prot_encoder"] = self.prot_encoder.to_dict()
        output["lig_encoder"] = self.lig_encoder.to_dict()
        return output
